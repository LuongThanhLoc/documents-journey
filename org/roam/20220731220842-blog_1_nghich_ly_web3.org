:PROPERTIES:
:ID:       9869dc53-fcc3-48f1-a6aa-5da5ba395344
:END:
#+title: Blog 1 - Nghịch lý Web3

* "Nghịch Lý Web3"
- Trong thế giới Web3: Công nghệ phi tập trung, Thứ người dân muốn. Giá trị ứng
  dụng, thứ người dân muôn. Cả 2 rất khó để kết hợp với nhau.
- Bài viết này Vũ sẽ cố gắng phân tích khoảng cách về hệ sinh thái web3 mà chúng
  ta đang hình dung và xây dựng hệ sinh thái thực tế, và trên cơ sở này khám phá
  một con đường để phá vỡ sự đột phá.
* Nghịch lý
- Trong cơn điên cuồng về khái niệm Web3 quét qua thế giới, một loạt các dự án
  DApp đã bén rễ và mọc như măng sau cơn mưa và được nuôi dưỡng bởi niềm tin vào
  những ý tưởng về phi tập trung.
  + NFT
  + DEX/ SWAP
  + USDC/DAI
  + FileCoin
  + Lab DAO
  + Flow
  + Aave
  + Gold
- Tuy nhiên sau vài năm phát triển thì những gì chúng ta thấy là CloudFlare sụp
  đổ, các nhà tiên tri về thị trường ngừng hô hào, các sàn giao dịch Phi tập
  trung không thể khớp lệnh... Và hầu hết các dự án đều sụp đổ thành cát bụi. Số
  tiền các nhà đầu tư vào các dự án bây thành mây khói.
- Vậy thì tại sao công nghệ Web3 lại như vậy?
- Tại sao kiến trúc cơ bản phi tập trung không thể hỗ trợ các kịch bản ứng dụng
  phù hợp với thói quen sữ dụng của người dân.
- Tại sao khái niệm nâng cấp blockchain không thể ra đời với các ứng dụng như
  twitter hay facebook?
- Tại sao một thế giới tiền điện tử tập trung vào quyền riêng tư của người dùng
  không thể mang lại trải nghiệm mượt mà.
- Mặc dù có rất nhiều câu hỏi được đặt ra, các câu trả lời được nói nhưng thực
  tế là hầu như các leader trong lĩnh vữc blockchain vẫn không ngừng suy nghĩ:
  + Đâu là khoảng cách giữa thiết kế phi tập trung lý tưởng và công trình hệ
    sinh thái ứng dụng thực tế.
