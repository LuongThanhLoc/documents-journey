
# Table of Contents

1.  [Understand About Blockchain Developer.](#org11b90ce)
2.  [Why?](#orgf1954c3)
3.  [Technical Position](#org4faa6af)
4.  [Choose A Platform](#orgd56e585)
5.  [DevOps](#orga8a7836)
6.  [Bussiness Domain Positioning](#org068a9c3)
    1.  [Games & `Metaverse`](#org2da5f40)
    2.  [DeFi](#org1c317ff)
7.  [Keep Learning&#x2026;](#orge6d4268)



<a id="org11b90ce"></a>

# Understand About Blockchain Developer.

-   Blockchain
-   Smart Contract


<a id="orgf1954c3"></a>

# Why?

-   Salary
    -   1000$ - 5000$
-   Future
    -   10-15 year
-   Low supply, high demand
-   Know how to invest safely.


<a id="org4faa6af"></a>

# Technical Position

-   Front-end
    -   Languages
        -   HTML/CSS
        -   Javascript
        -   Typescript
        -   `Rust`
        -   `WebAssembly`
    -   Framework
        -   ReactJs
        -   React Native
        -   NextJs
    -   Actions
        -   `Wallet Connection`
        -   `Communication with SmartContract`

-   Back-end
    -   Languages
        -   `Rust`
        -   Solidity/Viper
        -   Haskell
        -   Go
    -   Technical
        -   `Security Challengers`
        -   Testing SmartContract
        -   Optimization SmartContract
        -   Optimization Gas Fee


<a id="orgd56e585"></a>

# Choose A Platform

-   Rust Languages
    -   `Near`
    -   `Solana`
    -   Layer Zero - Substrate
    -   ICP
    -   FileCoin
    -   &#x2026;.
-   Solidity
    -   `Ethereum`
    -   `Binance Smart Chain`


<a id="orga8a7836"></a>

# DevOps

-   How to Deploy SmartContract
-   Deploy Dapp & Connect with Wallet
-   Handle privates key security
-   Hyper Ledger
-   Privates Blockchain


<a id="org068a9c3"></a>

# Bussiness Domain Positioning


<a id="org2da5f40"></a>

## Games & `Metaverse`

-   How to Faster?
-   How to lower Gas Fee?
-   Sự chuyển dịch từ Application Desktop -> Web Application.
-   Performance
    -   WebAssembly/ AssemblyScript


<a id="org1c317ff"></a>

## DeFi

-   DeFi Protocols
-   Oracles
-   Stable Coin
-   Mining
-   Staking
-   FlashLoan
-   DAO
-   DEX
-   Compound


<a id="orge6d4268"></a>

# Keep Learning&#x2026;

